<?php
/**
 * Created by PhpStorm.
 * User: Andrei
 * Date: 9/29/2018
 * Time: 12:14 PM
 */

class home extends Model {
	const id = 'home';

	public function __construct() {
		parent::__construct();

		$system_data = file_get_contents( App::$BASE_DIR . "/data/categories.json" );
		$this->items = json_decode( $system_data, true );

		if(isset($_GET['cat'])){
			foreach($this->items as $item){
				if($_GET['cat'] == $item['id']){
					echo "<h2>".$item['label']."</h2>";
				}
			}
		}
	}

	protected function _populateItem( $item_data, $item ) {
		$link = "//$_SERVER[HTTP_HOST]$_SERVER[PHP_SELF]?p=gallery&id=" . $item_data['id'];
		$item = str_replace( '{{link}}', $link, $item );

		return str_replace( '{{label}}', $item_data['label'], $item );
	}
}
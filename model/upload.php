<?php
/**
 * Created by PhpStorm.
 * User: Andrei
 * Date: 9/29/2018
 * Time: 12:14 PM
 */

if ( isset( $_POST['cat_id'] ) ) {
	require_once '../Model.php';
	require_once 'gallery.php';
}

class upload extends gallery {
	const id = 'upload';
	const BASE_DIR = '..';
	protected $TITLE = '';

	public function __construct() {
		if ( isset( $_POST['cat_id'] ) ) {
			$this->_uploadPhoto();
		}

		parent::__construct();
	}

	public function decorate( &$html ) {
		parent::decorate( $html );

		$url  = "//$_SERVER[HTTP_HOST]" . str_replace( 'index.php', 'model/upload.php', $_SERVER['PHP_SELF'] );
		$html = str_replace( '{{url}}', $url, $html );
	}

	private function _uploadPhoto() {
		$cat_id = $_POST['cat_id'];

		if ( ! is_null( $_FILES ) || $_FILES['photo_file']['size'] < 500000 ) {
			$photo_label = strip_tags( trim( $_POST['photo_label'] ) );
			$photo_raw   = $_FILES['photo_file'];
			$type = (substr_count('jpg', $photo_raw['type']) ? 'jpg' : false );
			if(!$type){
				$type = (substr_count('png', $photo_raw['type']) ? 'png' : false );
			}
			if($type) {
				/*
		array (size=1)
		  'photo_file' =>
			array (size=5)
			  'name' => string 'referendum.PNG' (length=14)
			  'type' => string 'image/png' (length=9)
			  'tmp_name' => string 'C:\xampp\tmp\phpE878.tmp' (length=24)
			  'error' => int 0
			  'size' => int 382083*/

				//todo check if gallery file exists, if not create one
				$gallery_file = self::BASE_DIR . "/data/gallery_" . $cat_id . ".json";
				$system_data  = file_get_contents( $gallery_file );
				$photos       = json_decode( $system_data, true );
				$new_id       = $this->_generateNewPhotoId( $photos );
				$photos[]     = [
					'id'    => $new_id,
					'label' => $photo_label
				];

				$system_data = json_encode( $photos );
				file_put_contents( $gallery_file, $system_data );
			}
		}

		header( 'Location: ../index.php?p=gallery&id=' . $cat_id );
	}

	private function _generateNewPhotoId( $photos ) {
		$new_id = 0;

		foreach ( $photos as $photo ) {
			if ( $photo['id'] > $new_id ) {
				$new_id = (int) $photo['id'];
			}
		}
		$new_id ++;

		return $new_id;
	}
}

if ( isset( $_POST['cat_id'] ) ) {
	new upload();
}
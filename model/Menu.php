<?php
/**
 * Created by PhpStorm.
 * User: Andrei
 * Date: 9/29/2018
 * Time: 10:17 AM
 */

class Menu extends Model {

	const id = 'menu';
	protected $items;
	protected $content = '{{menu}}';

	public function __construct($page_id) {
		parent::__construct();

		$this->items = App::getSysData( 'menu', $page_id )['options'];

	}

	protected function _populateItem( $item_data, $item ) {
		$link = "//$_SERVER[HTTP_HOST]$_SERVER[PHP_SELF]?p=" . $item_data['id'];
		$item = str_replace( '{{link}}', $link, $item );

		return str_replace( '{{label}}', $item_data['label'], $item );
	}

}
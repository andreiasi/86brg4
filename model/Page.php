<?php
/**
 * Created by PhpStorm.
 * User: Andrei
 * Date: 9/29/2018
 * Time: 10:17 AM
 */

class Page extends Model {

	const id = 'page';

	public function decorate( &$html) {
		$this->partial_data = App::getSysData( 'partial', static::id );
		$html = $this->_getTemplate();
	}



}
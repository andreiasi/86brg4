# PHP Photo Gallery App `86BrG4`
A small app built using a MVC structure. The purpose is to have an organized, modular and reusable code, while requiring a small number of dependencies.
- fully functional using `vanilla PHP`
- it uses `json files` instead of a database
- `bootstrap`

# User can
- list galleries,
- list pictures in gallery (as thumbnails),
- view individual photo,
- create, edit and delete galleries,
- upload and delete photos from gallery.

**On the page that lists the galleries:**
- it shows the title of each gallery;
- a cover photo of a gallery.

**On the gallery page:**
- it shows photo thumbnails;
- user can view individual photo in a bigger size.

# You can see the demo here 
**http://86brg4.xiis.ca**
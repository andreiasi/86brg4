<?php
/**
 * Created by PhpStorm.
 * User: Andrei
 * Date: 9/29/2018
 * Time: 12:14 PM
 */

if ( isset( $_POST['cat_id'] ) ) {
	require_once '../Model.php';
	require_once 'gallery.php';
}

class edit extends gallery {
	const id = 'edit';
	const BASE_DIR = '..';

	public function __construct() {
		if ( isset( $_POST['cat_id'] )
		     && strlen( $_POST['cat_id'] )
		     && strlen( $_POST['cat_label'] )
		) {
			$this->_editCategory();
		}

		parent::__construct();
	}

	public function decorate( &$html ) {
		parent::decorate( $html );

		$url  = "//$_SERVER[HTTP_HOST]" . str_replace( 'index.php', 'model/edit.php', $_SERVER['PHP_SELF'] );
		$html = str_replace( '{{url}}', $url, $html );
	}

	private function _editCategory() {
		$cat_id      = (int) $_POST['cat_id'];
		$system_data = file_get_contents( self::BASE_DIR . "/data/categories.json" );
		$categories  = json_decode( $system_data, true );

		foreach ( $categories as $index => $category ) {
			if ( (int) $category['id'] == $cat_id ) {
				$categories[ $index ]['label']       = strip_tags( trim( $_POST['cat_label'] ) );
				$categories[ $index ]['description'] = strip_tags( trim( $_POST['cat_description'] ) );
			}
		}

		$system_data = json_encode( $categories );
		file_put_contents( self::BASE_DIR . "/data/categories.json", $system_data );
		header( 'Location: ../index.php?p=gallery&id=' . $cat_id );
	}
}

if ( isset( $_POST['cat_id'] ) ) {
	new edit();
}
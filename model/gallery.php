<?php
/**
 * Created by PhpStorm.
 * User: Andrei
 * Date: 9/29/2018
 * Time: 12:14 PM
 */

class gallery extends Model {
	const id = 'gallery';
	protected $TITLE = '';
	protected $DESCRIPTION = '';

	public function __construct() {
		parent::__construct();

		$this->items = [];

		if ( isset( $_GET['id'] ) ) {
			$system_data = file_get_contents( App::$BASE_DIR . "/data/categories.json" );
			$categories  = json_decode( $system_data, true );
			$category_data = $this->_getCategoryData( $categories, (int) $_GET['id'] );
			if ( ! empty( $category_data ) ) {
				// set page title and description
				$this->TITLE       = $category_data['label'];
				$this->DESCRIPTION = $category_data['description'];
				// retrieve images
				$file_path = App::$BASE_DIR . "/data/gallery_" . $_GET['id'] . ".json";
				if ( file_exists( $file_path ) ) {
					$system_data = file_get_contents( $file_path );
					$this->items = json_decode( $system_data, true );
				}
			}
		}

	}

	public function decorate( &$html ) {
		parent::decorate( $html );

		$html = str_replace( '{{gallery_title}}', $this->TITLE, $html );
		$html = str_replace( '{{description}}', $this->DESCRIPTION, $html );
		$html = str_replace( '{{cat_id}}', $_GET['id'], $html );
	}

	protected function _getCategoryData( $categories, $id ) {
		$category_data = null;
		foreach ( $categories as $item ) {
			if ( $id == $item['id'] ) {
				$category_data = $item;
				break;
			}
		}

		return $category_data;
	}

	protected function _populateItem( $item_data, $item ) {
		/*$link = "//$_SERVER[HTTP_HOST]$_SERVER[PHP_SELF]?cat=" . $item_data['id'];
		$item = str_replace( '{{link}}', $link, $item );

		return str_replace( '{{label}}', $item_data['label'], $item );*/
	}
}
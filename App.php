<?php
/**
 * Created by PhpStorm.
 * User: Andrei
 * Date: 9/29/2018
 * Time: 8:27 AM
 */

class App {

	static $SYSTEM;
	static $BASE_DIR;
	private $default_page_id = 'home';

	public function __construct() {
		App::$BASE_DIR = __DIR__;
		$system_data   = file_get_contents( App::$BASE_DIR . "/data/system.json" );
		//todo: validation
		App::$SYSTEM = json_decode( $system_data, true );

		$this->_render();
	}

	public static function checkPartialId( $partial_id ) {
		return (bool) App::getSysData( 'partial', $partial_id );
	}

	public static function getSysData( $category, $id ) {

		foreach ( App::$SYSTEM[ $category ] as $data ) {
			if ( $data['id'] == $id ) {
				return $data;
			}
		}

	}

	private function _render() {
		if ( ! isset( $_GET['p'] ) || ! App::checkPartialId( $_GET['p'] ) ) {
			$page_id = $this->default_page_id;
		} else {
			$page_id = $_GET['p'];
		}

		$page_data = App::getSysData( 'partial', $page_id );
		$page      = new Page();
		$menu      = new Menu( $page_id );
		$content   = new $page_data['id']();

		$html = '';
		$page->decorate( $html );
		$menu->decorate( $html );
		$content->decorate( $html );

		echo $html;
	}


}
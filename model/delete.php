<?php
/**
 * Created by PhpStorm.
 * User: Andrei
 * Date: 9/29/2018
 * Time: 12:14 PM
 */

if ( isset( $_POST['cat_id'] ) ) {
	require_once '../Model.php';
}

class delete extends Model {
	const id = 'delete';
	const BASE_DIR = '..';

	public function __construct() {
		if ( isset( $_POST['cat_id'] ) ) {
			$this->_deleteCategory();
		}

		parent::__construct();
	}

	public function decorate( &$html ) {
		parent::decorate( $html );

		$url  = "//$_SERVER[HTTP_HOST]" . str_replace( 'index.php', 'model/delete.php', $_SERVER['PHP_SELF'] );
		$html = str_replace( '{{url}}', $url, $html );
		$html = str_replace( '{{cat_id}}', $_GET['id'], $html );
	}

	private function _deleteCategory() {
		$cat_id      = (int) $_POST['cat_id'];
		$system_data = file_get_contents( self::BASE_DIR . "/data/categories.json" );
		$categories  = json_decode( $system_data, true );

		foreach ( $categories as $index => $category ) {
			if ( (int) $category['id'] == $cat_id ) {
				unset( $categories[ $index ] );
				unlink( self::BASE_DIR . "/data/gallery" . $cat_id . ".json" );
			}
		}

		$system_data = json_encode( $categories );
		file_put_contents( self::BASE_DIR . "/data/categories.json", $system_data );
		header( 'Location: ../index.php' );
	}
}

if ( isset( $_POST['cat_id'] ) ) {
	new delete();
}
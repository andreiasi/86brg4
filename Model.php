<?php
/**
 * Created by PhpStorm.
 * User: Andrei
 * Date: 9/29/2018
 * Time: 10:10 AM
 */

class Model {

	const id = null;
	protected $items;
	protected $content = '{{content}}';
	protected $partial_data = null;

	public function __construct() {
	}


	public function decorate( &$html ) {
		if ( ! App::checkPartialId( static::id ) ) {
			return false;
		}
		$this->partial_data = App::getSysData( 'partial', static::id );

		$template = $this->_getTemplate();
		//set title
		if ( isset( $this->partial_data['label'] ) ) {
			$html = str_replace( '{{title}}', $this->partial_data['label'], $html );
		}
		// populate items
		$template = $this->_populateItems( $template );
		$html     = str_replace( $this->content, $template, $html );
	}

	protected function _populate() {
		throw new Error( "Missing populate method" );
	}

	protected function _getTemplate() {
		return file_get_contents( App::$BASE_DIR . "/view/" . $this->partial_data['template'] );
	}

	protected function _populateItems( $template ) {
		if ( substr_count( $template, '{{item}}' ) > 0 ) {
			$data   = explode( '{{item}}', $template );
			$data_2 = explode( '{{item_end}}', $data[1] );
			$item   = $data_2[0];

			$template = $data[0];
			if ( ! empty( $this->items ) ) {
				foreach ( $this->items as $item_data ) {
					$template .= $this->_populateItem( $item_data, $item );
				}
			}
			$template .= $data_2[1];
		}

		return $template;
	}


	protected function _populateItem( $item_data, $item ) {
		throw new Error( "Missing populate item method" );
	}

}
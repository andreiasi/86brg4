<?php
/**
 * Created by PhpStorm.
 * User: Andrei
 * Date: 9/29/2018
 * Time: 12:14 PM
 */

if ( isset( $_POST['cat_label'] ) ) {
	require_once '../Model.php';
}

class create extends Model {
	const id = 'create';
	const BASE_DIR = '..';

	public function __construct() {
		if ( isset( $_POST['cat_label'] ) && strlen( $_POST['cat_label'] ) ) {
			$this->_addNewCategory();
		}

		parent::__construct();
	}

	public function decorate( &$html ) {
		parent::decorate( $html );

		$url  = "//$_SERVER[HTTP_HOST]" . str_replace( 'index.php', 'model/create.php', $_SERVER['PHP_SELF'] );
		$html = str_replace( '{{url}}', $url, $html );
	}

	private function _addNewCategory() {
		$system_data = file_get_contents( self::BASE_DIR . "/data/categories.json" );
		$categories  = json_decode( $system_data, true );
		$new_id      = $this->_generateNewCategoryId( $categories );

		$categories[] = [
			'id'          => $new_id,
			'label'       => strip_tags( trim( $_POST['cat_label'] ) ),
			'description' => strip_tags( trim( $_POST['cat_description'] ) )
		];


		$system_data = json_encode( $categories );
		file_put_contents( self::BASE_DIR . "/data/categories.json", $system_data );
		file_put_contents( self::BASE_DIR . "/data/gallery_" . $new_id . ".json", [] );
		header( 'Location: ../index.php?p=gallery&id=' . $new_id );
	}

	private function _generateNewCategoryId( $categories ) {

		$new_id = 0;

		foreach ( $categories as $category ) {
			if ( $category['id'] > $new_id ) {
				$new_id = (int) $category['id'];
			}
		}
		$new_id ++;

		return $new_id;
	}
}

if ( isset( $_POST['cat_label'] ) ) {
	new create();
}